public class InputObject {

    private String Task_ID;
    private String logicalPageNumber;
    private String physAddress;

    public InputObject(String Task_ID, String logicalPageNumber, String physAddress){
        this.Task_ID = Task_ID;
        this.logicalPageNumber = logicalPageNumber;
        this.physAddress = physAddress;
    }

    public String getLogicalPageNumber() {
        return logicalPageNumber;
    }

    public String getTask_ID() {
        return Task_ID;
    }

    public String getPhysAddress() {
        return physAddress;
    }

    public void setLogicalPageNumber(String logicalPageNumber) {
        this.logicalPageNumber = logicalPageNumber;
    }

    public void setPhysAddress(String physAddress) {
        this.physAddress = physAddress;
    }

    public void setTask_ID(String task_ID) {
        Task_ID = task_ID;
    }

    public String toString(){
        return "T"+ this.Task_ID + ": " + this.logicalPageNumber + ": " + this.physAddress;
    }
}
