# README #

This is a multi-threaded project written in Java Programming Language which simulates a simple demand
paging memory management scheme. There are a total of 4 threads that run concurrently and are synchronized accordingly. These threads provide the following functionalities:
- Thread 1 runs the Producer Task. It reads the data of pages from a file and adds them to one buffer each(in total there are 2 buffers)
- Thread 2 and Thread 3 are Loader Tasks. They load these data from the buffers to their respective description tables(there are in total 2 tables)
- Thread 4 runs the Consumer Task. It checks constantly checks if a page should be removed from a page(in the case it finished its job)

