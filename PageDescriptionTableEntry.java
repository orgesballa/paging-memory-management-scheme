public class PageDescriptionTableEntry{

    private String physAddress;
    private String PageInMemoryStatus;
    private int  LoadTime;

    public PageDescriptionTableEntry(String physAddress, String PageInMemoryStatus, int LoadTime){
        this.physAddress = physAddress;
        this.PageInMemoryStatus = PageInMemoryStatus;
        this.LoadTime = LoadTime;
    }

    public String getPhysAddress() {
        return physAddress;
    }

    public void setPhysAddress(String physAddress) {
        this.physAddress = physAddress;
    }

    public String getPageInMemoryStatus() {
        return PageInMemoryStatus;
    }

    public void setPageInMemoryStatus(String pageInMemoryStatus) {
        PageInMemoryStatus = pageInMemoryStatus;
    }

    public int getLoadTime() {
        return LoadTime;
    }

    public void setLoadTime(int loadTime) {
        LoadTime = loadTime;
    }

    @Override
    public String toString(){
        return this.physAddress + ": " + this.PageInMemoryStatus + ": " + this.LoadTime;
    }

    public boolean equals(Object o) {
            PageDescriptionTableEntry p = (PageDescriptionTableEntry) o;
            if(this.physAddress.equals(p.physAddress))
                return true;
            else
                return false;
        }
    }