import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class Page_Load_Buffer {
    private static final int CAPACITY = 6; // buffer size
    private java.util.LinkedList<InputObject> queue =
            new java.util.LinkedList<>();
    private int id;

    public Page_Load_Buffer(int id){this.id = id;}
    // Create a new lock
     static Lock lock = new ReentrantLock();

    // Create two conditions
    private static Condition notEmpty = lock.newCondition();
    private static Condition notFull = lock.newCondition();

    public void add(InputObject x) {
        lock.lock(); // Acquire the lock
        try {
            while (queue.size() == CAPACITY) {
                System.out.println("Page_Load_Buffer" + this.id +" is full");
                notFull.await();
            }
            queue.offer(x);
            notEmpty.signal(); // Signal notEmpty condition
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        } finally {
            lock.unlock(); // Release the lock
        }
    }

    public InputObject read() {
        InputObject x=null;
        lock.lock(); // Acquire the lock
        try {
            while (queue.isEmpty()) {
                notEmpty.await();
            }
            x = queue.remove();
            notFull.signal(); // Signal notFull condition
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        } finally {
            lock.unlock(); // Release the lock
            return x;
        }

    }
}
