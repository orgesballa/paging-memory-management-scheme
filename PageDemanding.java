import java.io.PrintWriter;
import java.util.concurrent.*;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;


public class PageDemanding {

    private static ArrayBlockingQueue<InputObject> buffer1 = new ArrayBlockingQueue<InputObject>(5); //Buffer to store Input Object
    private static ArrayBlockingQueue<InputObject> buffer2 = new ArrayBlockingQueue<InputObject>(5); //Buffer to store Input Object
    private static ArrayList<String> lines = new ArrayList<>(); //This serves as an arrayList for reading the files
    private static int i = 0;
    private static ArrayList<InputObject> inputObjects = new ArrayList<>(); // This serves as an arrayList that stores every input
    private static ArrayBlockingQueue<PageDescriptionTableEntry> table1 = new ArrayBlockingQueue<>(16); // Our table of entries, capacity 16
    private static ArrayBlockingQueue<PageDescriptionTableEntry> table2 = new ArrayBlockingQueue<>(8); // Second table of entries, capacity 8
    private static int Counter1 = 0; // Counter that will be used by Checker Thread and LoaderThread 1 to check if it has 5 pages at a time
    private static int Counter2 = 0; // Counter 2 that will be used by Checker Thread and LoaderThread 2 to check if it has 5 pages at a time
    private static int LoadTime1 = 0; // Just a counter for Loading Times
    private static int LoadTime2 = 0;
    private static Lock lock1 = new ReentrantLock(); // Lock 1 will serve as a lock for Loader Task 1 and Checker 1
    private static Lock lock2 = new ReentrantLock(); // Lock 2 will serve as a lock for Loader Task 2 and Checker 2
    private static Condition counter1Condition = lock1.newCondition(); // This condition will be used by lock1 to check if the condition is fulfilled
    private static Condition counter2Condition = lock2.newCondition();
    private static volatile boolean exit = false; // This variable serves as a quit condition, since the thread keeps running because of while(true)
    // condition, so I added this variable to stop the while loop and exit the program
    private static int order = 1;  // I added this order to make sure that they finish in order. By trial and error, I noticed that sometimes Checker
    // Thread would stop before LoaderThread and LoaderThreads would be WAIT all the time
    // By this I make sure that Producer finishes first, then either LoaderThread 1 or LoaderThread 2, and in the end CheckerThreads
    private static PrintWriter writer;

    public static void main(String[] args) {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(args[0])); // Get the arguments which is the input.txt
            lines.add(reader.readLine()); // Just a simple ReadLines
            while (lines.get(i) != null) {
                lines.add(reader.readLine());
                i++;
            }
            reader.close();
        } catch (IOException e) {
            System.out.println("Please enter a valid path as run arguments");
        }

        for (int i = 0; i < lines.size() - 1; i++) {
            InputObject a = new InputObject(Character.toString(lines.get(i).charAt(1)), // Create InputObjects and put them in an ArrayList
                    (Character.toString(lines.get(i).charAt(4))),
                    lines.get(i).substring(7, 13));
            inputObjects.add(a); // This AL will be used later for producerTask
        }

        try {
            writer = new PrintWriter("output.txt"); // PrintWriter that will write in output.txt
        } catch (IOException e) {
            writer.println("IOException ");
        }
/*

        // Other approach in executing the tasks
        ExecutorService executor = Executors.newFixedThreadPool(4);
        executor.execute(new ProducerTask());
        executor.execute(new Loader_Task_1());
        executor.execute(new Loader_Task_2());
        executor.execute(new Checker_Task());
        executor.shutdown();
*/

        Thread producerThread = new Thread(new ProducerTask()); // Create a producerThread
        producerThread.setName("Producer Thread"); // Set the name
        Thread loader_Thread_1 = new Thread(new Loader_Task_1()); // Create the LoaderThread 1
        loader_Thread_1.setName("Loader Thread 1"); // Set the name
        Thread loader_Thread_2 = new Thread(new Loader_Task_2()); // Create the LoaderTHread 2
        loader_Thread_2.setName("Loader Thread 2"); // Set the name
        Thread checker_Thread = new Thread(new Checker_Task()); // Create the CheckerThread
        checker_Thread.setName("Checker Thread"); // Set the name
        producerThread.start();   //Start the threads
        loader_Thread_1.start();
        loader_Thread_2.start();
        checker_Thread.start();

        /*
          To make sure we close the PrintWriter, we need to do it after all threads have stopped working.
          If we don't close the writer, the last buffer will be lost and the log will not be correct.
          So we use Thread.join(), to make sure that they wait until all threads are finished and then we run
          our statements
        */
        try {
            producerThread.join();
            loader_Thread_1.join();
            loader_Thread_2.join();
            checker_Thread.join();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        writer.println("Program Ended");
        writer.close();
    }


    /* Producer Task will read from InputObjects and check if it belongs to
       Task 1 or Task2. Since Buffer1 and Buffer2 are using ArrayBlockingQueues, there is
       no need for synchronization since it is already handled. This eases our work a lot
     */

    static class ProducerTask implements Runnable {
        @Override
        public void run() {
            try {
                for (int i = 0; i < inputObjects.size(); i++) {
                    if (i == inputObjects.size() - 1) {
                        writer.println("-------------" + Thread.currentThread().getName() + " finished-----------");
                        order++;
                        exit = true;
                    }
                    if (inputObjects.get(i).getTask_ID().equals("1")) { // I added ID so I can distinguish tasks
                        writer.println(Thread.currentThread().getName() + ": Adding information to Page Load Buffer 1 " + inputObjects.get(i).toString());
                        buffer1.put(inputObjects.get(i)); // Using put method to add the objects in the buffer
                        writer.println("Current size of buffer1 is:" + buffer1.size());
                    } else {
                        writer.println(Thread.currentThread().getName() + ": Adding information to Page Load Buffer 2 " + inputObjects.get(i).toString());
                        buffer2.put(inputObjects.get(i));
                        writer.println("Current size of buffer2 is:" + buffer2.size());
                    }
                }
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }

    /*
        Loader_Task_1 is the task responsible for reading from Buffer 1 and storing them in Table1
     */
    static class Loader_Task_1 implements Runnable {
        @Override
        public void run() {
            while (!exit || !buffer1.isEmpty() || !(order == 2 || order == 3)) { // This way we can make the thread stop and not run infinitely
                try {                                           // !exit is always true until produerThread finishes
                    // buffer1.isEmpty() means if buffer1 has finished working
                    // order is used to make sure Checker finishes the last
                    InputObject x = buffer1.take(); // As explained in JAVADOC, take() removes the head of the queue, but also waits
                    // until an element becomes available
                    LoadTime1++;                    // Increase the LoadTime by 1
                    PageDescriptionTableEntry e = new PageDescriptionTableEntry(x.getPhysAddress(), x.getLogicalPageNumber(), LoadTime1);
                    // Create an entry object;
                    if (table1.contains(e)) {       // Check if exists already
                        writer.println(e.toString() + " exists in table1");
                        writer.println("Updating the loading time");
                        for (PageDescriptionTableEntry o : table1) {
                            if (o.getPhysAddress().equals(e.getPhysAddress())) {
                                o.setLoadTime(e.getLoadTime());   // Update the Loading Time
                            }
                        }

                    } else {
                        table1.put(e);  // Otherwise just put it in the table
                        Counter1++;     // Increment the counter since we added one
                        writer.println(Thread.currentThread().getName() + ": Loader_Task_1 puts: " + x.toString() + " to Table 1 ");
                        writer.println("Table 1 has size : " + table1.size());
                        writer.println("Counter 1 has value:" + Counter1);

                    }
                    lock1.lock();  // Acquire the lock because this is critical section
                    writer.println(Thread.currentThread().getName() + " acquiring lock 1");
                    while (Counter1 > 5) {  // Our condition given in the requirements, no more than 5 pages at a time, so if counter > 5
                        counter1Condition.await();  // we wait for the condition (Signal)
                    }
                    writer.println(Thread.currentThread().getName() + " releasing lock 1");
                    lock1.unlock();
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
            order++;
            writer.println("-------------" + Thread.currentThread().getName() + " finished-----------");
        }

        public Iterator<PageDescriptionTableEntry> findIndex(PageDescriptionTableEntry x) {
            while (!table1.iterator().next().equals(x)) {
                table1.iterator().next();
            }
            return table1.iterator();
        }

    }

    /*
       Loader_Task_2 is the task responsible for reading from Buffer 2 and storing them in Table2
    */
    static class Loader_Task_2 implements Runnable {

        @Override
        public void run() {
            while (!exit || !buffer2.isEmpty() || !(order == 2 || order == 3)) { // Same logic as above
                try {
                    InputObject x = buffer2.take(); // Remove the head of the buffer2
                    LoadTime2++; // Increment the LoadTime
                    PageDescriptionTableEntry e = new PageDescriptionTableEntry(x.getPhysAddress(), x.getLogicalPageNumber(), LoadTime2);

                    if (table2.contains(e)) { // If exists, just update the loadtime
                        writer.println(e.toString() + " exists in table2");
                        writer.println("Updating the loading time");
                        for (PageDescriptionTableEntry o : table2) {
                            if (o.getPhysAddress().equals(e.getPhysAddress())) {
                                o.setLoadTime(e.getLoadTime());
                            }
                        }

                    } else { // Otherwise put it in the table
                        table2.put(e);
                        Counter2++;   // Increment the counter since we add a new one
                        writer.println(Thread.currentThread().getName() + ": Loader_Task_2 loads: " + x.toString() + " to table 2");
                        writer.println("Table 2 has size : " + table2.size());
                        writer.println("Counter 2 has value:" + Counter2);

                    }
                    lock2.lock();
                    writer.println(Thread.currentThread().getName() + " acquiring lock 2");
                    while (Counter2 > 5) {
                        counter2Condition.await();
                    }
                    lock2.unlock();
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
            order++;
            writer.println("-------------" + Thread.currentThread().getName() + " finished-----------");
        }

        public Iterator<PageDescriptionTableEntry> findIndex2(PageDescriptionTableEntry x) {
            while (!table2.iterator().next().equals(x)) {
                table2.iterator().next();
            }
            return table2.iterator();
        }

    }

    static class Checker_Task implements Runnable {
        @Override
        public void run() {
            while (!exit || !buffer1.isEmpty() || !buffer2.isEmpty() || order != 4) {  // Checking until all threads are done
                if (Counter1 > 5) {     // One common mistake would be to lock and then check. That would result in DeadLock
                    lock1.lock();       // Only if counter is > 5, we get the lock, and proceed
                    try {
                        writer.println(Thread.currentThread().getName() + ": Loader thread 1 achieved the maximum count");
                        Counter1 -= 1; // Decrement the counter
                        writer.println(Thread.currentThread().getName() + ": Removing from table 1: " + table1.take().toString());
                        // Remove using .take() from table the head of the queue
                        counter1Condition.signalAll(); // signal the waiting threads on this condition
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    } finally {
                        lock1.unlock(); // Release the lock
                    }

                }
                if (Counter2 > 5) {   // Some procedure for second table and second counter
                    lock2.lock();
                    try {
                        writer.println(Thread.currentThread().getName() + ": Loader thread 2 achieved the maximum count");
                        Counter2 -= 1;
                        writer.println(Thread.currentThread().getName() + ": Removing from table 2: " + table2.take().toString());
                        counter2Condition.signalAll();
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    } finally {
                        lock2.unlock(); // Release the lock
                    }
                }
            }
            order++;
            writer.println("-------------" + Thread.currentThread().getName() + " finished-----------");
        }
    }
            /*
        public PageDescriptionTableEntry findMin2(){
            ArrayList<PageDescriptionTableEntry> tableCopy = new ArrayList<>();
            Collections.copy(table2,tableCopy);
            Collections.sort(tableCopy);
            return tableCopy.get(0);
            }

        public PageDescriptionTableEntry findMin1(){
            ArrayList<PageDescriptionTableEntry> tableCopy = new ArrayList<>();
            Collections.copy(table1,tableCopy);
            Collections.sort(tableCopy);
            return tableCopy.get(0);
        }*/
}
